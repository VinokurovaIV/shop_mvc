package shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.model.entities.Category;
import shop.model.repositories.CategoriesRepository;
import shop.model.repositories.ProductsRepository;

import java.math.BigDecimal;


@Controller
public class CatalogController {

    private final CategoriesRepository categoriesRepository;
    private final ProductsRepository productsRepository;

    @Autowired
    public CatalogController(CategoriesRepository categoriesRepository, ProductsRepository productsRepository) {
        this.categoriesRepository = categoriesRepository;
        this.productsRepository = productsRepository;
    }

    @RequestMapping("/catalog")
    public String redirect() {
        return "redirect:/catalog/1";
    }

    @RequestMapping("/catalog/{category_id}")
    public String display(
            @PathVariable(name="category_id", required = false)
                    Integer categoryId,
            @RequestParam(name="page", required = false)
                    Integer pageNum,
            @RequestParam(name="searchPhrase", required = false)
                    String searchPhrase,
            @RequestParam(name="sortDirect", required = false)
                    String sortDirect,
            @RequestParam(name="minPrice", required = false)
                    BigDecimal minPrice,
            @RequestParam(name="maxPrice", required = false)
                    BigDecimal maxPrice,
            Model model) {

        Category currentCategory = categoriesRepository.findById(categoryId).orElse(null);

        if (currentCategory==null) {
            return "redirect: /catalog/1";
        }
    }
    )
}
