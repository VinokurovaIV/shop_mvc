package shop.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name="product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    @Column(name="id")
    Integer id;
    @Column(name="image_url")
    String imageURL;
    @Column(name="name")
    String name;
    @Column(name="description")
    String description;
    @Column(name="price")
    BigDecimal price;

    @ManyToOne
    @JoinColumn(name="category_id")
    Category category;

    public Product() {
    }

    public Product(Integer id, String name, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }


}
